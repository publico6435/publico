package main

import (
	"context"
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"regexp"
	"runtime"
	"strconv"
	"sync"
	"time"

	"github.com/google/uuid"
	amqp "github.com/rabbitmq/amqp091-go"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	/*addr := "amqp://guest:guest@localhost:5672/"
	queue := "coladeprueba"*/

	addr := "amqps://Teleservicios:21FQzU3QL6ObBn1y@sb-export-altenar2-stage.biahosted.com:5671//Teleservicios"
	queue := "Teleservicios_Bets"

	timeNameLog := time.Now().Format("2006-01-02")
	runLogFile, _ := os.OpenFile(
		"consumer-rabbitmq-"+timeNameLog+".log",
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0664,
	)
	multi := zerolog.MultiLevelWriter(os.Stdout, runLogFile)
	log.Logger = zerolog.New(multi).With().Timestamp().Logger()

	log.Info().Msg("Hello World!")

	sigsVar := make(chan os.Signal, 1)
	rmq := New(queue, addr, sigsVar)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5000)
	defer cancel()

	for {
		err := rmq.Stream(ctx)
		if errors.Is(err, ErrDisconnected) {
			continue
		}
		break
	}
}

var (
	ErrDisconnected = errors.New("desconectado de rabbitmq, intentando conectar")
)

// Estructura que contiene todos los datos para gestionar el consumo de elementos de la cola
type Client struct {
	streamQueue   string
	connection    *amqp.Connection
	channel       *amqp.Channel
	done          chan os.Signal
	notifyClose   chan *amqp.Error
	notifyConfirm chan amqp.Confirmation
	isConnected   bool
	alive         bool
	threads       int
	wg            *sync.WaitGroup
}

// New is a constructor that takes address, push and listen queue names, logger, and a channel that will notify rabbitmq client on server shutdown. We calculate the number of threads, create the client, and start the connection process. Connect method connects to the rabbitmq server and creates push/listen channels if they don't exist.
func New(streamQueue, addr string, done chan os.Signal) *Client {
	threads := runtime.GOMAXPROCS(0)
	if numCPU := runtime.NumCPU(); numCPU > threads {
		threads = numCPU
	}

	client := Client{
		threads:     threads,
		streamQueue: streamQueue,
		done:        done,
		alive:       true,
		wg:          &sync.WaitGroup{},
	}

	go client.handleReconnect(addr)
	return &client
}

// handleReconnect will wait for a connection error on
// notifyClose, and then continuously attempt to reconnect.
func (c *Client) handleReconnect(addr string) {
	for c.alive {
		c.isConnected = false
		t := time.Now()
		log.Info().Msg("LOG.Intentando conectar a rabbitMQ: " + addr)
		var retryCount int
		for !c.connect(addr) {
			if !c.alive {
				return
			}
			select {
			case <-c.done:
				return
			case <-time.After(5*time.Second + time.Duration(retryCount)*time.Second):
				log.Info().Msg("No se pudo reconectar. Se intentara en un momento.")
				retryCount++
			}
		}
		log.Info().Msg("Se realizo la conexion a rabbitMQ en: " + strconv.FormatInt(time.Since(t).Milliseconds(), 10) + "ms")
		select {
		case <-c.done:
			return
		case <-c.notifyClose:
		}
	}
}

// connect will make a single attempt to connect to
// RabbitMq. It returns the success of the attempt.
func (c *Client) connect(addr string) bool {
	conn, err := amqp.Dial(addr)
	if err != nil {
		log.Info().Msg("failed to dial rabbitMQ server: " + err.Error())
		return false
	}
	ch, err := conn.Channel()
	if err != nil {
		log.Info().Msg("failed connecting to channel: " + err.Error())
		return false
	}
	ch.Confirm(false)
	_, err = ch.QueueDeclarePassive(
		c.streamQueue,
		true,  // Durable
		true,  // Delete when unused
		false, // Exclusive
		false, // No-wait
		nil,   // Arguments
	)
	if err != nil {
		log.Info().Msg("failed to declare stream queue: " + err.Error())
		return false
	}

	c.changeConnection(conn, ch)
	c.isConnected = true
	return true
}

// changeConnection takes a new connection to the queue,
// and updates the channel listeners to reflect this.
func (c *Client) changeConnection(connection *amqp.Connection, channel *amqp.Channel) {
	c.connection = connection
	c.channel = channel
	c.notifyClose = make(chan *amqp.Error)
	c.notifyConfirm = make(chan amqp.Confirmation)
	c.channel.NotifyClose(c.notifyClose)
	c.channel.NotifyPublish(c.notifyConfirm)
}

func (c *Client) Stream(cancelCtx context.Context) error {
	c.wg.Add(c.threads)

	for {
		if c.isConnected {
			break
		}
		time.Sleep(1 * time.Second)
	}

	err := c.channel.Qos(1, 0, false)
	if err != nil {
		return err
	}

	var connectionDropped bool

	for i := 1; i <= c.threads; i++ {
		log.Info().Msg("Entre al bucle. thread: " + strconv.Itoa(i))
		msgs, err := c.channel.Consume(
			c.streamQueue,
			strconv.Itoa(i), // Consumer
			false,           // Auto-Ack
			false,           // Exclusive
			false,           // No-local
			false,           // No-Wait
			nil,             // Args
		)
		if err != nil {
			log.Error().Msg("Entre al bucle. thread: " + strconv.Itoa(i) + "Y se produjo un error : " + err.Error())
			return err
		}

		go func(i int) {
			log.Info().Msg("=========Llamando a la gorutine para processar result thread: " + strconv.Itoa(i))
			defer c.wg.Done()
			for {
				select {
				case <-cancelCtx.Done():
					log.Info().Msg("===Se Cancelo el trabajo del thread: " + strconv.Itoa(i))
					return
				case msg, ok := <-msgs:
					log.Info().Msg("===Se procesara el resultado del thread: " + strconv.Itoa(i))
					if !ok {
						log.Info().Msg("===Se procesara el resultado del thread: " + strconv.Itoa(i) + " Se cancela el trabajo porque OK is :" + strconv.FormatBool(ok))
						connectionDropped = true
						return
					}
					c.parseEvent(cancelCtx, msg)
				}
			}
		}(i)
	}
	log.Info().Msg("################## FIN DE LAS ITERACIONES DEL BUCLE antes del wait")
	c.wg.Wait()
	log.Info().Msg("################## FIN DE LAS ITERACIONES DEL BUCLE despues del wait")

	if connectionDropped {
		return ErrDisconnected
	}

	return nil
}

type event struct {
	Job            string      `json:"Job"`
	Identificador  string      `json:"Identificador"`
	TimeStamp      string      `json:"TimeStamp"`
	AutoSettle     bool        `json:"AutoSettle"`
	BetTransaction interface{} `json:"BetTransaction"`
}

func (c *Client) parseEvent(cancelCtx context.Context, msg amqp.Delivery) {
	startTime := time.Now()

	var evt event
	err := json.Unmarshal(msg.Body, &evt)
	//log.Error().Msg("msg.Body: " msg.Body)
	if err != nil {
		msg.Nack(false, false)
		log.Error().Msg("ftook-ms: " + strconv.FormatInt(time.Since(startTime).Milliseconds(), 10) + " unmarshalling body\n: " + err.Error())
		return
	}

	if evt.TimeStamp == "" {
		msg.Nack(false, false)
		log.Info().Msg("ftook-ms: " + strconv.FormatInt(time.Since(startTime).Milliseconds(), 10) + " error received event without data\n: ")
		return
	}

	defer func(ctx context.Context, e event, m amqp.Delivery) {
		if err := recover(); err != nil {
			stack := make([]byte, 8096)
			stack = stack[:runtime.Stack(stack, false)]
			log.Info().Bytes("stack", stack).Str("level", "fatal").Interface("error", err).Msg("panic recovery for rabbitMQ message")
			msg.Nack(false, false)
		}
	}(cancelCtx, evt, msg)

	// Este switch fue modificado
	var commandPat = regexp.MustCompile(`^[0-9]+`)
	switch cmd := msg.ConsumerTag; {
	case commandPat.MatchString(cmd):
		evt.Job = msg.ConsumerTag
		evt.Identificador = generateKey()
		//#########################################################
		// Insertando en la BD
		serverAPI := options.ServerAPI(options.ServerAPIVersion1)
		opciones := options.Client().ApplyURI("mongodb://localhost:27017").SetServerAPIOptions(serverAPI)
		// Create a new client and connect to the server
		client, err := mongo.Connect(context.TODO(), opciones)
		log.Info().Msg("Iniciando conexion:..." + evt.Identificador + " msg.ConsumerTag:..." + msg.ConsumerTag)
		if err != nil {
			panic(err)
		}
		defer func() {
			err := client.Disconnect(context.TODO())
			log.Info().Msg("Cerrando conexion:..." + evt.Identificador + " msg.ConsumerTag:..." + msg.ConsumerTag)
			if err != nil {
				panic(err)
			}
		}()

		coll := client.Database("ejemplo").Collection("bet")

		result, err := coll.InsertOne(context.TODO(), evt)

		if err != nil {
			fmt.Println("InsertOne ERROR:", err)
			os.Exit(1) // safely exit script on error
		}

		fmt.Println("InsertOne() API result:", result)
	default:
		msg.Reject(false)
		strRechazado := fmt.Sprintf("%v", evt)
		log.Error().Msg("el mensaje fue rechazado:..." + msg.ConsumerTag + " " + strRechazado)
		return
	}

	log.Info().Int64("took-ms", time.Since(startTime).Milliseconds()).Msgf("%s succeeded", evt.Job)
	msg.Ack(false)
}

func (c *Client) Close() error {
	if !c.isConnected {
		return nil
	}
	c.alive = false
	log.Info().Msg("Waiting for current messages to be processed...")
	c.wg.Wait()
	for i := 1; i <= c.threads; i++ {
		log.Info().Msg("Closing consumer: " + strconv.Itoa(i))
		err := c.channel.Cancel("consumer: "+strconv.Itoa(i), false)
		if err != nil {
			return fmt.Errorf("error canceling consumer %s: %v", strconv.Itoa(i), err)
		}
	}
	err := c.channel.Close()
	if err != nil {
		return err
	}
	err = c.connection.Close()
	if err != nil {
		return err
	}
	c.isConnected = false
	log.Info().Msg("gracefully stopped rabbitMQ connection")
	return nil
}

func generateKey() string {

	bv := []byte(uuid.New().String())

	hasher := sha1.New()
	hasher.Write(bv)
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))

	return sha
}
